class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKCYAN = '\033[96m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


# DAC0, DAC1, EEPROM, TestpulseMux
names = ["D0", "D1", "EE", "MX", "E2"]
start_ad = [0x21, 0x22, 0x51, 0x4c, 0x73]

# Those are the values we will use to XOR the addresses with
xor_val = [0x00, 0x04, 0x08, 0x04, 0x1C, 0x04, 0x08, 0x04]


cur_adr = list(start_ad)

# lets make some lists for tracking
adr_list = []
uniq = {0x70: ["CLK0"]}

# iterate over all XOR values and apply them in series
for backbone_id, x in enumerate(xor_val):
    new_adr = []
    # for each address in the list
    for dev, adr in enumerate(cur_adr):
        # do the xor calculation
        xx = adr^x
        new_adr.append(xx)

        # check if there are collisions
        if not xx in uniq:
            uniq[xx] = [f"{names[dev]}-{backbone_id}"]
        else:
            uniq[xx].append(f"{names[dev]}-{backbone_id}")
            print(f'Duplicare addr {xx}: {uniq[xx]}')

    # add to list of addresses
    cur_adr = list(new_adr)
    adr_list.append(new_adr)

print("Board XOR values:")
for board in xor_val[1:]:
    print(f"{hex(board)}, ", end="")
print("\n")

# output results
print("Each row contains the addresses of the 4 chips we have on each backbone")
print(names)
for entry in adr_list:
    print(list(map(hex, entry)))


print("\n\n== Address space ==")
for row in range(0,127,16):
    for itm in range(16):
        #print(f"{hex(row+itm)} ", end="")
        if (row+itm) in uniq:
            if len(uniq[row+itm]) > 1:
                print((bcolors.FAIL + "%4s " + bcolors.ENDC) % uniq[row+itm][0], end="")
            else:
                print((bcolors.WARNING + "%4s " + bcolors.ENDC) % uniq[row+itm][0], end="")
        else:
            print("0x%02x " % (row+itm), end="")
    print("")
