// I²C Scanner for Hodoscope Backbone
// This program is used for the testbench for the PCB

#include <Wire.h>

uint8_t extra_device = 0;
bool dac_0x21 = false;
bool dac_0x22 = false;
const float target_voltage = 29.5;

void setup() {
  // join i2c bus (address optional for master)
  Wire.begin();        
  // start serial for output
  Serial.begin(115200);  
}

// Calculate the DAC code for a given voltage
// Warning: no bounds checking is done here
uint16_t DAC_num(float V_set) {
  //   == Calculation ==
  //   V_ref = 11.5V
  //   V_DAC = DAC / 2^10 ? * 2500
  //   V_sum = (V_ref + V_DAC)/2
  //   V_bias = V_sum*4.3
  //   DAC = (VBias*2/5 - V_ref)*2^10/2.5
  return (uint16_t)(round((V_set * 2 / 4.3 - 11.5) * 1024 / 2.5) );
}

// Set the bias voltage of one channel
// * voltage: between 24.7V and 30.1V
// * address: i²c address of the LTC2635 DAC chip
int set_voltage(uint8_t address, float voltage) {
  uint16_t val = DAC_num(voltage) << 6;
  Wire.beginTransmission(address);
  Wire.write(0b00111111); // Write and update all channels
  Wire.write(val / 256);
  Wire.write(val % 256);  
  Wire.endTransmission();
}


// prints 8-bit data in hex with leading zeroes
void PrintHex8(uint8_t data) {
        Serial.print(" 0x"); 
        if (data<0x10) {Serial.print("0");} 
        Serial.print(data,HEX); 
}

// Helper function that sets various flags based on
// the I²C address
uint8_t check_extra_device(uint8_t address) {
  switch (address) {
    case 0x21:
      dac_0x21 = true;
      return 0;
    case 0x22:
      dac_0x22 = true;
      return 0;
    case 0x51:
    case 0x4c:
    case 0x73:
      return 0;
    default:
      return address;
  }
}

void loop()
{
  byte error, address;
  int nDevices;

  // reset counters
  nDevices = 0;
  dac_0x21 = false;
  dac_0x21 = false;
  extra_device = 0;

  Serial.print("\n\nScanning and showing 7-bit addresses (SDA=A4, SCL=A5)\n");
  Serial.print("      0x00 0x01 0x02 0x03 0x04 0x05 0x06 0x07 0x08 0x09 0x0A 0x0B 0x0C 0x0D 0x0E 0x0F");
  for(address = 0; address < 127; address++ )
  {
    // start new line on every 16 scans
    if ((address) % 16 == 0) {
      Serial.print("\n");
      PrintHex8(address);
    }

    // skip the general-call addresses
    if (address < 8) {
      Serial.print("     ");
      continue;
    }
    
    // The i2c_scanner uses the return value of
    // the Write.endTransmisstion to see if
    // a device did acknowledge to the address.
    Wire.beginTransmission(address);
    error = Wire.endTransmission();
 
    if (error == 0)
    {
      PrintHex8(address); 
      nDevices++;
      extra_device = max(extra_device, check_extra_device(address));
    } else if (error==4) {
      Serial.print(" EEEE");
    } else {
      Serial.print(" ----");
    }
  }
  Serial.print("\nFound ");
  Serial.print(nDevices);
  Serial.println(" devices on the bus");


  // set DAC voltages if chip was found
  // we want this so we can measure the voltage with a multimeter to
  // verify that the DAC and OpAmp are working as intended
  if (dac_0x21) {
    Serial.println("Setting DAC0 taget voltage");
    set_voltage(0x21, target_voltage);
  }
  if (dac_0x22) {
    Serial.println("Setting DAC1 taget voltage");
    set_voltage(0x22, target_voltage);
  }

  // We know the chip on the extra board has the address 0x52.
  // If a 'unknown' chip was found during the scan, this is most likely
  // that chip. If we XOR its address with 0x52 we can guess the programmed
  // XOR value of the LTC4316 address-translator.
  if (extra_device != 0) {
    Serial.print("XOR value is ");
    PrintHex8(extra_device ^ 0x52);
    Serial.print("\n");    
  } else {
    Serial.print("XOR value is unknown. Something is wrong.\n");
  }

  // wait 5 seconds for next scan
  delay(5000);           
}
