# 8-Channel SiPM Backbone

Backbone PCB that connects 8 SiPM-Frontends to the MVT Mezzaine card

# System Overview
The full hodoscope will have two crossed layes with 16 scintillator-strips each.
For every scintillator, we will connect two SiPM to allow for coincidence measurements. So in total 64 SiPM channels need to be supported.

Building boards that support a multiple of 8 channels is the logical choice. DACs and I²C-Bus-expanders will easily work with 8 channels as well.


## Goals & Requirements
* Connect to 8 [SiPM-Frontent-Modules](https://git.rwth-aachen.de/muon-hodoscope/sipm-front-end) 
* Exact fit to the mechanical structure of the hodoscope
* Layout to allow daisy-chaining of supply- and data lines
* 8-Channel I²C-Portexpander to mask test-pulse outputs
* I²C DAC to control the SiPM bias voltage for each SiPM
  * We will need 8 DAC-Channels for that, perhaps the 4-Channel DAC from the [mezzaine board](https://git.rwth-aachen.de/muon-hodoscope/octa-channel-mvt-mezzanine) can be reused
  * A DAC with 8 channels would mean that the Voltage-reference-signal needs to be routed a long way. Here less channels is better.
  * The voltage-range of interest is between 25V and 30V, so we want to offset the DAC value somehow
  * Use a op-amp voltage follower or similar for this, the current is way below 1mA
* I²C address-translation ([LTC4316](https://www.analog.com/media/en/technical-documentation/data-sheets/4316fa.pdf) or similar) to allow multiple of those boards on the same bus
  * clever I²C address-space layout, e.g. everything within a 0x04 wide address-space
  * If boards are dasiy-chained, we can always shift the address by 0x04
* RC-Filter for the +5V supply voltage for each frontend (tbd. since there are filters on the frontend PCB already)
* LED-Indicator for +32V and +5V

### Connectors
* 8x SiPM-Frontend via pogo-pins
* 8x SiMP pulse output via MCX-Jack (SMT)
* 1x Testpulse input via MCX-Jack
* Power and I²C Connectors, ideally on the board edge so they can be daisy chained
  * SCL + SDA
  * GND
  * +5V
  * +32V

## Legal
**NoAI**: This project may not be used in datasets for, in the development of, or as inputs to generative AI programs.

**§44b**: Dies ist ein in maschinenlesbarer Form vorliegender Nutzungsvorbehalt entsprechend §44b UrhG

**License**: [CERN-OHL-S](LICENSE)
* 2024 cpresser
